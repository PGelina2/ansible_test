#!/usr/bin/python3

import urllib
import httplib2
from xml.dom import minidom

baseurl = 'https://localhost:8089'
userName = 'admin'
password = 'Diet1coke*'
output = 'csv'  #options are: raw, csv, xml, json, json_cols, json_rows
                # If you are using "table" in your search result, you must(?) use "csv"

searchQuery = 'earliest=-2d index=* EventCode=4624 | head 3 | table _time,host,user,EventCode'

# Authenticate with server.
# Disable SSL cert validation. Splunk certs are self-signed.
try:
    serverContent = httplib2.Http(disable_ssl_certificate_validation=True).request(baseurl + '/servicesNS/auth/login','POST', headers={}, body=urllib.parse.urlencode({'username':userName, 'password':password}))[1]
except:
    print("error in retrieving login.")

print(minidom.parseString(serverContent).toprettyxml(encoding='UTF-8'))

# Remove leading and trailing whitespace from the search
searchQuery = searchQuery.strip()

# If the query doesn't already start with the 'search' operator or another 
# generating command (e.g. "| inputcsv"), then prepend "search " to it.

print("----- RESULTS BELOW -----")

# Run the search.
# Again, disable SSL cert validation. 
searchResults = httplib2.Http(disable_ssl_certificate_validation=True).request(baseurl + '/services/search/jobs/export?output_mode='+output,'POST',urllib.parse.urlencode({'search': searchQuery}))[1]

searchResults = searchResults.decode('utf-8')

for result in searchResults.splitlines():
    print(result)
    print("---") # These are just here to demonstrate that we are reading line-by-line

